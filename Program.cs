﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace TPR3
{
    class Program
    {
        public static readonly double[] _probability = new double[] { 0.5, 0.3, 0.2 };

        static void Main(string[] args)
        {
            int[,] matrix;

            IEnumerable<string> lines = File.ReadLines("C:\\Users\\Katrin\\Desktop\\Навчання\\4 курс\\Теорія прийняття рішень\\Лаб3\\TPR3\\matrix.txt");

            var matrixSize = lines.Count();

            matrix = new int[matrixSize, matrixSize];


            for (int i = 0; i < lines.Count(); i++)
            {
                var numbers = lines.ToArray()[i].Split(" ");

                for (int j = 0; j < numbers.Length; j++)
                {
                    matrix[i, j] = int.Parse(numbers[j]);
                }
            }

            CalculateBaesLaplass(matrix);
            CalculateHodjaLemana(matrix);
            CalculateHermejera(matrix);
        }

        static void CalculateBaesLaplass(int[,] matrix)
        {
            double? maxValue = null;
            int? index = null;
            for (int i = 0; i < matrix.GetLongLength(0); i++)
            {
                double sum = 0;
                for (int j = 0; j < matrix.GetLongLength(1); j++)
                {
                    sum += matrix[i, j] * _probability[i];
                }

                if (!maxValue.HasValue || maxValue < sum)
                {
                    maxValue = sum;
                    index = i;
                }
            }

            Console.WriteLine($"Baes-Laplass - {maxValue} - #{index + 1}");
        }

        static void CalculateHodjaLemana(int[,] matrix)
        {
            double[,] ap = new double[matrix.GetLongLength(0), matrix.GetLongLength(1)];

            for (int i = 0; i < matrix.GetLongLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLongLength(1); j++)
                {
                    ap[i,j] = matrix[i, j] * _probability[j];
                }
            }

            double[] sum = new double[ap.GetLongLength(0)];
            for (int i = 0; i < ap.GetLongLength(0); i++)
            {
                for (int j = 0; j < ap.GetLongLength(1); j++)
                {
                    sum[i] += ap[i, j];
                }
            }

            double?[] minValue = new double?[matrix.GetLongLength(0)];
            for (int i = 0; i < matrix.GetLongLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLongLength(1); j++)
                {
                    if (!minValue[i].HasValue || matrix[i, j] < minValue[i])
                        minValue[i] = matrix[i, j];
                }
            }

            int? index = null;
            double? maxValue = null;
            for (int i = 0; i < matrix.GetLongLength(1); i++)
            {
                double? temp = 0.5 * sum[i] + (1 - 0.5) * minValue[i];
                if (!maxValue.HasValue || temp > maxValue)
                {
                    maxValue = temp;
                    index = i;
                }
            }

            Console.WriteLine($"HodjaLemana - {maxValue} - #{index + 1}");
        }
        static void CalculateHermejera(int[,] matrix)
        {
            double[,] hermejeraMatrix = new double[matrix.GetLongLength(0), matrix.GetLongLength(1)];

            for (int i = 0; i < matrix.GetLongLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLongLength(1); j++)
                {
                    if (matrix[i, j] > 0)
                    {
                        hermejeraMatrix[i, j] = matrix[i, j] / _probability[j];
                    }
                    else
                    {
                        hermejeraMatrix[i, j] = matrix[i, j] * _probability[i];
                    }
                }
            }

            double? maxValue = null;
            int? index = null;
            for (int i = 0; i < hermejeraMatrix.GetLongLength(0); i++)
            {
                double? miinValue = null;
                for (int j = 0; j < hermejeraMatrix.GetLongLength(1); j++)
                {
                    if (!miinValue.HasValue || hermejeraMatrix[i, j] < miinValue)
                    {
                        miinValue = hermejeraMatrix[i,j];
                    }
                }

                if (!maxValue.HasValue || miinValue > maxValue)
                {
                    maxValue = miinValue;
                    index = i;
                }
            }

            Console.WriteLine($"Hermejera - {maxValue} - #{index + 1}");
        }
    }
}
